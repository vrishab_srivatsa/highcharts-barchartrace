var select_input = document.getElementById('dimension');
function plot(){
	var initialData;
	var chart;
	var startYear = "1",
		endYear = "26",
		btn = document.getElementById('play-pause-button'),
		input = document.getElementById('play-range');

	var selected_dim = select_input.value;

	var title_text = selected_dim.toUpperCase();
	var json_path = "data_" + selected_dim + ".json";
	var slice_by;
	if(selected_dim == "pmt"){
		slice_by = 5;
	} else {
		slice_by = 10;
	}

	months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	/**
	 * Animate dataLabels functionality
	 */
	(function (H) {
		var FLOAT = /^-?\d+\.?\d*$/;

		// Add animated textSetter, just like fill/strokeSetters
		H.Fx.prototype.textSetter = function (proceed) {
			var startValue = this.start.replace(/ /g, ''),
				endValue = this.end.replace(/ /g, ''),
				currentValue = this.end.replace(/ /g, '');

			if ((startValue || '').match(FLOAT)) {
				startValue = parseInt(startValue, 10);
				endValue = parseInt(endValue, 10);

				// No support for float
				currentValue = Highcharts.numberFormat(
					Math.round(startValue + (endValue - startValue) * this.pos), 0);
			}

			this.elem.endText = this.end;

			this.elem.attr(
				this.prop,
				currentValue,
				null,
				true
			);
		};

		// Add textGetter, not supported at all at this moment:
		H.SVGElement.prototype.textGetter = function (hash, elem) {
			var ct = this.text.element.textContent || '';
			return this.endText ? this.endText : ct.substring(0, ct.length / 2);
		}

		// Temporary change label.attr() with label.animate():
		// In core it's simple change attr(...) => animate(...) for text prop
		H.wrap(H.Series.prototype, 'drawDataLabels', function (proceed) {
			var ret,
				attr = H.SVGElement.prototype.attr,
				chart = this.chart;

			if (chart.sequenceTimer) {
				this.points.forEach(
					point => (point.dataLabels || []).forEach(
						label => label.attr = function (hash, val) {
							if (hash && hash.text !== undefined) {
								var text = hash.text;

								delete hash.text;

								this.attr(hash);

								this.animate({
									text: text
								});
								return this;
							} else {
								return attr.apply(this, arguments);
							}
						}
					)
				);
			}


			ret = proceed.apply(this, Array.prototype.slice.call(arguments, 1));

			this.points.forEach(
				p => (p.dataLabels || []).forEach(d => d.attr = attr)
			);

			return ret;

		});
	})(Highcharts);

	/**
	 * Calculate the data output
	 */


	function getData(year) {
		let output = Object.entries(initialData).map(row => {
			var [i, MID] = row;
			var start = slice_by*(Number(year) - 1)
			if(i >= start && i < start + slice_by){
				return [MID["MID"], Number(MID["Volume"]), MID["Month"]]
			}
		}).sort((a, b) => b[1] - a[1]);
		var top_sum = 0;
		for(var count = 0; count < slice_by; count++){
			top_sum = top_sum + output[count][1];
		}
		return ([top_sum, output.slice(0, slice_by), output[0][2]]);
	}

	Highcharts.getJSON(json_path, function(data) {
		initialData = data;
		chart = Highcharts.chart('container', {
			chart: {
				animation: {
					duration: 2000
				},
				width: 1280,
				height: 800,
				events: {
					render() {
						let chart = this;
						// Responsive input
						input.style.width = chart.plotWidth - chart.legend.legendWidth + chart.plotLeft / 2 - 10 + 'px' // where 10 is a padding
					}
				},
				marginRight: 50,
			},
			plotOptions: {
				series: {
					animation: false,
					groupPadding: 0,
					pointPadding: 0.1,
					borderWidth: 0
				}
			},
			title: {
				useHTML: true,
				text: `Successful Orders (Top ${slice_by})- Overall: <b>${getData(startYear)[0]}</b>`
			},

			legend: {
				align: 'right',
				verticalAlign: 'bottom',
				itemStyle: {
					fontWeight: 'bold',
					fontSize: '50px',
				},
				symbolHeight: 0.001,
				symbolWidth: 0.001,
				symbolRadius: 0.001,
			},
			xAxis: {
				type: 'category',
			},
			yAxis: [{
				opposite: true,
				title: {
					text: 'Order Volume by ' + title_text
				},
				tickAmount: 10
			}],
			series: [{
				colorByPoint: true,
				dataSorting: {
					enabled: true,
					matchByName: true
				},
				type: 'bar',
				dataLabels: [{
					enabled: true,
				}],
				name: getData(startYear)[2],
				data: getData(startYear)[1]
			}]
		});
	});

	/**
	 * Update the chart. This happens either on updating (moving) the range input,
	 * or from a timer when the timeline is playing.
	 */
	function update(increment) {
		if (increment) {
			input.value = parseInt(input.value) + increment;
		}
		if (input.value >= parseInt(endYear)) { // Auto-pause
			pause(btn);
		}

		chart.update({
			title: {
				useHTML: true,
				text: `<div>Successful Orders (Top ${slice_by})- Overall: <b>${getData(input.value)[0]}</b></span></div>`
			},
		}, false, false, false)

		chart.series[0].update({
			name: getData(input.value)[2],
			data: getData(input.value)[1]
		})
	}

	/**
	 * Play the timeline.
	 */
	function play(button) {
		button.title = 'pause';
		button.className = 'fa fa-pause';
		chart.sequenceTimer = setInterval(function () {
			update(1);
		}, 2000);

	}

	/** 
	 * Pause the timeline, either when the range is ended, or when clicking the pause button.
	 * Pausing stops the timer and resets the button to play mode.
	 */
	function pause(button) {
		button.title = 'play';
		button.className = 'fa fa-play';
		clearTimeout(chart.sequenceTimer);
		chart.sequenceTimer = undefined;
	}


	btn.addEventListener('click', function () {
		if (chart.sequenceTimer) {
			pause(this)
		} else {
			play(this)
		}
	})
	/** 
	 * Trigger the update on the range bar click.
	 */
	input.addEventListener('click', function () {
		update()
	})
}

$(document).ready(function(){
	plot()
})

select_input.addEventListener('change', function(){
	$('#play-controls').replaceWith('<div id="play-controls"><button id="play-pause-button" class="fa fa-play" title="play"></button><input id="play-range" type="range" value="1" min="1" max="26" /></div>')
	plot()
})