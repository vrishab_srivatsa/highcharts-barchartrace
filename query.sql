SELECT * EXCEPT(Rank, month_rank)
FROM(
    SELECT *, ROW_NUMBER() OVER(ORDER BY month_rank) as Iter FROM (
        SELECT *, DENSE_RANK() OVER(PARTITION BY Month ORDER BY Volume desc) as Rank FROM (
            SELECT 
                distinct count(case when payment_status = "SUCCESS" then order_id end) as Volume,
                coalesce({{dimension}}, 'Unknown') as MID,
                format_timestamp("%B %Y", parse_timestamp("%Y/%m/%d_%H:%M:%S", system_date, "UTC"), "Asia/Kolkata") as Month,
                format_timestamp("%Y/%m", parse_timestamp("%Y/%m/%d_%H:%M:%S", system_date, "UTC"), "Asia/Kolkata") as month_rank
            FROM 
                `express_checkout_sessions_v2.ec_sessions202*`
            GROUP BY MID, Month, month_rank
        )
        ORDER BY month_rank
    )
    WHERE Rank <= {{top_n}}
)